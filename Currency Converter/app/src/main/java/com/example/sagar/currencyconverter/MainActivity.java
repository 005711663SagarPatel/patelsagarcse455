package com.example.sagar.currencyconverter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void Convert(View view) {
        //USD = United States Dollar, Yen = Japanese Yen, YPD = Yen per Dollar
        double USD,Yen,YPD;
        // Current rate is 111.12
        YPD = 111.12;
        //dollarAmount will get the dollar amount user wants to convert
        // We will convert it using YPD and then displayView will display that converted Amount
        EditText dollarAmount = (EditText)findViewById(R.id.dollarInputEditText);
        TextView displayView = (TextView) findViewById(R.id.YenDisplay);
        USD = Integer.parseInt(dollarAmount.getText().toString());
        Yen = USD*YPD;
        displayView.setText(""+Yen+" Yen");


    }
}
