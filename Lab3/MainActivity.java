package com.example.sagar.currencyconverter2;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.HttpURLConnection;
import java.net.URL;



public class MainActivity extends AppCompatActivity {

    String currency_URL = "https://api.fixer.io/latest?base=USD";
    String  JSON = "";

    String line = "";
    String rate = "";

    protected ArrayAdapter<String> menuAdapter;
    Spinner currenMenu = (Spinner)findViewById(R.id.currencySpinner);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Spinner currencyMenu = (Spinner)findViewById(R.id.currencySpinner);
        menuAdapter = new ArrayAdapter<String>(MainActivity.this,
                android.R.layout.simple_spinner_dropdown_item,getResources().getStringArray(R.array.countryNames1));
        currencyMenu.setAdapter(menuAdapter);


    }

    public void Convert(View view) {
        // Will create an object of Background Task and will use it to
        // find the value of dollar amount in Yen and display it
        System.out.println("TESTING 1 ... Before AsynchExecution\n");
        BackgroundTask backgroundTask = new BackgroundTask();
        backgroundTask.execute();

        System.out.println("TESTING 2 ... After AsynchExecution\n");
    }


    private class BackgroundTask extends AsyncTask<Void, Void, String>{
        @Override
        protected void onPreExecute(){
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Void ... values){
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(String result){
            super.onPostExecute(result);
            TextView displayView = (TextView)findViewById(R.id.YenDisplay);
            EditText dollarAmount = (EditText)findViewById(R.id.dollarInputEditText);
            String dollar = dollarAmount.getText().toString();
            if(dollar.isEmpty()){
                displayView.setText(result);
            }else {
                displayView.setText("USD: " + dollarAmount.getText().toString() + " = " + result);
            }
        }

        @Override
        protected String doInBackground(Void ... params){
            String result = "";
            try{
                URL web_url = new URL(MainActivity.this.currency_URL);
                HttpURLConnection httpURLConnection = (HttpURLConnection)web_url.openConnection();
                httpURLConnection.setRequestMethod("GET");
                InputStream inputStream = httpURLConnection.getInputStream();
                Spinner currencyMenu = (Spinner)findViewById(R.id.currencySpinner);
                String countryName;
                System.out.println("\nTESTING ... BEFORE connection method to URL");

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                System.out.println("CONNECTION SUCCESSFUL");

                while(line!=null){
                    line = bufferedReader.readLine();
                    JSON = JSON + line;
                }

                System.out.println("\nTHE JSON: " + JSON);

                JSONObject obj = new JSONObject(JSON);

                JSONObject objRate = obj.getJSONObject("rates");

                countryName = currencyMenu.getSelectedItem().toString();
                System.out.println(countryName);
                rate = objRate.get(countryName).toString();

                Double rates = Double.parseDouble(rate);
                System.out.println("\nWhat is the rate: " + rate + "\n");
                System.out.println("\nTesting JSON String Exchange Rate INSIDE " +
                        "ASYNCTASK: (append this string with your variable of type " +
                        "double, obtained from the String variable, rate");

                // The code for the On Clicked Method Will Go here

                String USD;
                double dollar,yenAmount;

                EditText dollarAmount = (EditText)findViewById(R.id.dollarInputEditText);
                TextView displayView = (TextView) findViewById(R.id.YenDisplay);

                USD = dollarAmount.getText().toString();
                // Making sure that the dollarAmount Edit text is not empty
                if(USD == " " || USD == null || USD.isEmpty() ){
                    // Message for user to enter a number in
                    result = "This field Cannot be Empty";
                }else{
                    // Will Convert the string into double
                    dollar = Double.parseDouble(USD);
                    // Now will convert it into Japanese Yen and the display it into display View
                    yenAmount = dollar*rates;
                    result = Double.toString(yenAmount);
                }
            }catch(MalformedURLException e){
                e.printStackTrace();
            }catch(IOException e){
                e.printStackTrace();
            }catch(JSONException e){
                Log.e("MYAPP","unexpected JSON exception",e);
                System.exit(1);
            }

            return result;

        }

    }




}






